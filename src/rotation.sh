#!/bin/bash

# NOTE: rotation commands came from https://www.raspberrypi.org/forums/viewtopic.php?t=264107

if [ "$#" != 1 ]; then
    echo "Illegal number of parameters" >&2
    exit 1
fi

if [ "$1" = "--normal" ]; then
    echo "Rotating to NORMAL..."
    #xrandr --output DSI-1 --rotate normal
    #xinput --set-prop "FT5406 memory based driver" "Coordinate Transformation Matrix" 0 0 0 0 0 0 0 0 0
    echo "Done."
    exit 0
elif [ "$1" = "--left" ]; then
    echo "Rotating to LEFT..."
    #xrandr --output DSI-1 --rotate left
    #xinput --set-prop "FT5406 memory based driver" "Coordinate Transformation Matrix" 0 -1 1 1 0 0 0 0 1
    echo "Done."
    exit 0
elif [ "$1" = "--right" ]; then
    echo "Rotating to RIGHT..."
    #xrandr --output DSI-1 --rotate right
    #xinput --set-prop "FT5406 memory based driver" "Coordinate Transformation Matrix" 0 1 0 -1 0 1 0 0 1
    echo "Done."
    exit 0
elif [ "$1" = "--inverted" ]; then
    echo "Rotating to INVERTED..."
    #xrandr --output DSI-1 --rotate inverted
    #xinput --set-prop "FT5406 memory based driver" "Coordinate Transformation Matrix" -1 0 1 0 -1 1 0 0 1
    echo "Done."
    exit 0
else
    echo "Bad parameter" >&2
    exit 1
fi

#!/bin/bash

echo "Getting sudo access..."
sudo -v

echo "Removing program directory..."
sudo rm -rf /usr/local/share/rotationBuddy

echo "Removing program from PATH..."
sudo rm /usr/local/bin/rotation-buddy

echo "Removing program from menu..."
sudo rm /usr/share/applications/rotationBuddy.desktop

echo "Removing program from autostart..."
sudo rm $HOME/.config/autostart/rotationBuddy.desktop

echo "NOTE: Please reboot to stop Rotation Buddy."

echo "Done!"
exit
